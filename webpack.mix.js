let mix = require("laravel-mix");

mix.js("src/main.js", "dist/js/main.js")
  .setPublicPath('dist')
  .options({
    processCssUrls: false
  })
  .extract()
  .vue();
