// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import { createApp } from 'vue';
import App from './App.vue';
// import './components';
import router from './router';

var app = createApp({
  components: { App },
  template: '<App/>'
});

app.use(router);
app.mount('#app');
